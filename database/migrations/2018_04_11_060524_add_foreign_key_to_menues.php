<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMenues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menues', function (Blueprint $table) {
           $table->unsignedInteger('categorie_id')->nullable()->after('offer');
           $table->unsignedInteger('user_id')->nullable()->after('offer');
            $table->foreign('categorie_id')->references('id')->on('categories')
                ->onDelete('SET NULL')
                ->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menues', function (Blueprint $table) {
           $table->dropForeign('user_id');
           $table->dropForeign('categorie_id');
           $table->dropColumn('user_id');
           $table->dropColumn('categorie_id');
        });
    }
}
