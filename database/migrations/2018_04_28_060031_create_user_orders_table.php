<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('menu_id')->nullable();
            $table->unsignedInteger('quantity')->nullable();
            $table->unsignedInteger('delivery_details_id')->nullable();
            $table->date('order_date');
            $table->softDeletes();
            $table->foreign('menu_id')->references('id')
                ->on('menues')
                ->onDelete('set null')
                ->onUpdate('cascade');
            $table->foreign('delivery_details_id')
                ->references('id')
                ->on('delivery_details')
                ->onUpdate('cascade')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_orders');
    }
}
