<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        $categorie = new \App\Categorie();
        $categorie->title = 'Food1';
        $categorie->save();

        $categorie = new \App\Categorie();
        $categorie->title = 'Food2';

        $categorie->save();
        $categorie = new \App\Categorie();
        $categorie->title = 'Food3';
        $categorie->save();

        $categorie = new \App\Categorie();
        $categorie->title = 'Food4';
        $categorie->save();
    }
}
