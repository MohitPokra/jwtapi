<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class MenuesTableSeeder extends Seeder
{
    public function run()
    {
        $menue = new \App\Menue();
        $menue->title = 'menue1';
        $menue->price = 50;
        $menue->benefit = "Has to be defined";
        $menue->Description = "Has to be added";
        $menue->offer = "Not define yer";
        $menue->user_id = 1;
        $menue->categorie_id = 1;
        $menue->date = '2018-04-06';
        $menue->quantity = 4;
        $menue->save();

        $menue = new \App\Menue();
        $menue->title = 'menue1';
        $menue->price = 50;
        $menue->benefit = "Has to be defined";
        $menue->Description = "Has to be added";
        $menue->offer = "Not define yer";
        $menue->user_id = 1;
        $menue->categorie_id = 1;
        $menue->date = '2018-04-06';
        $menue->quantity = 4;
        $menue->save();

        $menue = new \App\Menue();
        $menue->title = 'menue1';
        $menue->price = 50;
        $menue->benefit = "Has to be defined";
        $menue->Description = "Has to be added";
        $menue->offer = "Not define yer";
        $menue->user_id = 1;
        $menue->categorie_id = 1;
        $menue->date = '2018-04-06';
        $menue->quantity = 4;
        $menue->save();

        $menue = new \App\Menue();
        $menue->title = 'menue1';
        $menue->price = 50;
        $menue->benefit = "Has to be defined";
        $menue->Description = "Has to be added";
        $menue->offer = "Not define yer";
        $menue->user_id = 1;
        $menue->categorie_id = 1;
        $menue->date = '2018-04-06';
        $menue->quantity = 4;
        $menue->save();

        $menue = new \App\Menue();
        $menue->title = 'menue1';
        $menue->price = 50;
        $menue->benefit = "Has to be defined";
        $menue->Description = "Has to be added";
        $menue->offer = "Not define yer";
        $menue->user_id = 1;
        $menue->categorie_id = 1;
        $menue->date = '2018-04-06';
        $menue->quantity = 4;
        $menue->save();

        $menue = new \App\Menue();
        $menue->title = 'menue1';
        $menue->price = 50;
        $menue->benefit = "Has to be defined";
        $menue->Description = "Has to be added";
        $menue->offer = "Not define yer";
        $menue->user_id = 1;
        $menue->categorie_id = 1;
        $menue->date = '2018-04-06';
        $menue->quantity = 4;
        $menue->save();

        $menue = new \App\Menue();
        $menue->title = 'menue1';
        $menue->price = 50;
        $menue->benefit = "Has to be defined";
        $menue->Description = "Has to be added";
        $menue->offer = "Not define yer";
        $menue->user_id = 1;
        $menue->categorie_id = 2;
        $menue->date = '2018-04-06';
        $menue->quantity = 4;
        $menue->save();

        $menue = new \App\Menue();
        $menue->title = 'menue1';
        $menue->price = 50;
        $menue->benefit = "Has to be defined";
        $menue->Description = "Has to be added";
        $menue->offer = "Not define yer";
        $menue->user_id = 1;
        $menue->categorie_id = 2;
        $menue->date = '2018-04-06';
        $menue->quantity = 4;
        $menue->save();

        $menue = new \App\Menue();
        $menue->title = 'menue1';
        $menue->price = 50;
        $menue->benefit = "Has to be defined";
        $menue->Description = "Has to be added";
        $menue->offer = "Not define yer";
        $menue->user_id = 1;
        $menue->categorie_id = 1;
        $menue->date = '2018-04-06';
        $menue->quantity = 3;
        $menue->save();

        $menue = new \App\Menue();
        $menue->title = 'menue1';
        $menue->price = 50;
        $menue->benefit = "Has to be defined";
        $menue->Description = "Has to be added";
        $menue->offer = "Not define yer";
        $menue->user_id = 1;
        $menue->categorie_id = 3;
        $menue->date = '2018-04-06';
        $menue->quantity = 4;
        $menue->save();
    }
}
