<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
          $user = new User();
          $user->name = 'mohit';
          $user->email= 'mohit@gmail.com';
          $user->password = \Illuminate\Support\Facades\Hash::make('123456');
          $user->remember_token = false;
          $user->phone_no = '9090909090';
          $user->address = 'what is address';
          $user->save();

        $user = new User();
        $user->name = 'nikhil';
        $user->email= 'nikhil@gmail.com';
        $user->password = \Illuminate\Support\Facades\Hash::make('123456');
        $user->remember_token = false;
        $user->phone_no = '90909109090';
        $user->address = 'what is address';
        $user->save();

        $user = new User();
        $user->name = 'gudan';
        $user->email= 'gudan@gmail.com';
        $user->password = \Illuminate\Support\Facades\Hash::make('123456');
        $user->remember_token = false;
        $user->phone_no = '9090955509090';
        $user->address = 'what is address';
        $user->save();
    }
}
