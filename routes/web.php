<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1',['namespace' => 'App\Http\Controllers', 'middleware' =>  [] ], function ($api) {
    $api->POST('auth/login', 'AuthController@login');
    $api->POST('auth/register', 'AuthController@register');
    $api->POST('auth/refresh', 'AuthController@refresh');
    $api->resource('categories', 'CategorieController');
    $api->resource('menu', 'MenueController');
});

$api->version('v1', ['namespace' => 'App\Http\Controllers', 'middleware' => ['api.auth', 'api.cors']], function ($api) {
     $api->resource('delivery-detail', 'DeliveryDetailController');
     $api->resource('user-order', 'UserOrderController');
     $api->GET('auth/logout', 'AuthController@logOut');
});