<?php

namespace App\Http\Controllers;

use App\DeliveryDetail;
use App\Http\Requests\DeliveryDetail\StoreRequest;
use App\Http\Requests\DeliveryDetail\UpdateRequest;
use Illuminate\Http\Request;

class DeliveryDetailController extends Controller
{
    public function store(StoreRequest $request) {
        $detail = new DeliveryDetail($request->all());
        $detail->save();
       return $this->response->array([
           'msg' => 'Added successfully',
           'id' => $detail->id
       ])->setStatusCode(200);
    }

    public function update(UpdateRequest $request, $id) {

         $detail = DeliveryDetail::findOrfail($id)->update($request->all());
         return $this->response->array([
               'msg' => 'Updated successfully',
               'id' =>    $id
          ])->setStatusCode(200);
    }


}
