<?php

namespace App\Http\Controllers;

use App\Http\Requests\LogInRequest;
use App\Http\Requests\LogOutRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Dingo\Api\Auth\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;

class AuthController extends Controller
{

    public function index () {
        return JWTAuth::parseToken()->toUser();
    }

    public function login (LogInRequest $request) {
        $credentials = $request->only(['email', 'password']);

        try {
            if(! $token = JWTAuth::attempt($credentials)) {
                return $this->response->errorUnauthorized();
            }
        }catch (JWTException $e) {
            return $this->response->errorInternal();
        }

        $user = User::where('email', '=', $request->email)->first();

        return $this->response->array([
            'msg' => 'Login successfull',
            'token' => $token,
            'user' => $user,
        ])->setStatusCode(200);
    }

    public function register (RegisterRequest $request) {

         $user = new User();
         $user->name = $request->name;
         $user->email = $request->email;
         $user->password = Hash::make($request->password);
         $user->phone_no = $request->phone_no;
         $user->address = $request->address;
         $user->save();

         $token = JWTAuth::fromUser($user);
         $user = JWTAuth::toUser($token);
         $array = [
             'token' => $token,
             'user'  => $user,
         ];
         return $this->response->array($array);
    }

    public function logOut () {
        return 'i am here';
        JWTAuth::invalidate(JWTAuth::getToken());
        return $this->response->array([
            'msg' => 'Logout successfull',
        ])->setStatusCode(200);
    }

    public function refresh () {

    }

}
