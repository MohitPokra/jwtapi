<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserOrder\StoreRequest;
use App\UserOrder;
use Illuminate\Http\Request;

class UserOrderController extends Controller
{
    public function store(StoreRequest $request)
    {
        $order = new UserOrder($request->all());
        $order->save();
        return $this->response->array([
            'msg' => 'Added successfully',
            'id' => $order->id
        ]);
    }
}
