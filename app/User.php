<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
     use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function DeliveryDetail () {
        return $this->hasMany(DeliveryDetail::class);
    }

    public function userOrder () {
        return $this->hasManyThrough(UserOrder::class, DeliveryDetail::class, 'user_id', 'delivery_details_id', 'id', 'id');
    }
}
