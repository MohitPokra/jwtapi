<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOrder extends Model
{
    public $timestamps = false;
    protected $fillable = [ 'id', 'menu_id', 'delivery_details_id', 'order_date', 'quantity'];

    public function deliveryDetail () {
        return $this->belongsTo(DeliveryDetail::class, 'delivery_details_id');
    }
}
