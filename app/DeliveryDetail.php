<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryDetail extends Model
{
    protected $fillable = [
        'name', 'email', 'address', 'phone_no', 'user_id'
    ];

    protected $hidden = [
        'user_id'
    ];

    public function user () {
        return $this->belongsTo(User::class);
    }

    public function userOrder () {
        return $this->hasMany(UserOrder::class, 'delivery_details_id');
    }
}
