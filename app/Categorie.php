<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    public function menue () {
        return $this->hasMany(Menue::class, 'categorie_id');
    }
}
