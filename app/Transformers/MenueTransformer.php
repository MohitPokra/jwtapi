<?php

namespace App\Transformers;
use App\Menue;
use League\Fractal\TransformerAbstract;


class MenueTransformer extends TransformerAbstract
{
    public function transform(Menue $menue) {
        return [
        'id' => (int) $menue->id,
        'title' => $menue->title,
        'benefit' => $menue->benefit,
        'description' => $menue->description,
        'status' => $menue->status,
        'offer' => $menue->offer,
        'image' => $menue->image,
        'date'  => $menue->date,
        'quantity' => $menue->quantity,
        'price' => $menue->price,
        ];
    }
}
