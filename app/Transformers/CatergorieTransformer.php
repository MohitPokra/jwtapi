<?php

namespace App\Transformers;
use App\Categorie;
use League\Fractal\TransformerAbstract;


class CatergorieTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'menue'
        ];

    public function transform(Categorie $categorie) {
        return [
            'id' => (int) $categorie->id,
            'title' => $categorie->title,
            'menue' => $categorie->menue
        ];
    }

    public function includeMenue(Categorie $categorie) {
        $menue = $categorie->menue;

          return $this->collection($menue, new MenueTransformer());
    }
}
